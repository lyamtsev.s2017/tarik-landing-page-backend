from django.db import models
from django_extensions.db.models import TimeStampedModel


class TSModel(TimeStampedModel):
    class Meta:
        ordering = ('created',)
        get_latest_by = 'created'
        abstract = True


class UserChoices(models.Model):
    class PositionChoices(models.IntegerChoices):
        BUSINESS_OWNER = 0, 'Business Owner'
        EMPLOYEE = 1, 'Employee'
        MANAGER = 2, 'Manager'
        DIRECTOR = 3, 'Director'
        VP = 4, 'VP'

        __empty__ = 'Empty'

    class RoleChoices(models.IntegerChoices):
        ENTREPRENEUR = 0, 'Entrepreneur'
        CONSULTANT = 1, 'Consultant'
        STRATEGY_OR_ANALYST = 2, 'Strategy / Analyst'
        SALES_OR_MARKETING = 3, 'Sales / Marketing'
        INVESTMENT_PROFESSIONAL = 4, 'Investment Professional'
        OPERATION = 5, 'Operation'
        IT = 6, 'IT'
        PRODUCT_MANAGER = 7, 'Product Manager'
        ACCOUNTING_MANAGEMENT = 8, 'Accounting Management'
        OTHERS = 9, 'Others'

        __empty__ = 'Empty'

    class IndustryChoices(models.IntegerChoices):
        AGRICULTURE = 0, 'Agriculture'
        CHEMICAL = 1, 'Chemical'
        IT_AND_COMPUTER = 2, 'IT & Computer'
        CONSTRUCTION = 3, 'Construction'
        ENERGY = 4, 'Energy'
        FINANCIAL_INSURANCE = 5, 'Financial Insurance'
        FMGG = 6, 'FMGG'
        OTHER = 7, 'Other'
        ENTERTAINMENT = 8, 'Entertainment'
        FOOD = 9, 'Food'
        HEALTH = 10, 'Health'
        HOSPITALITY = 11, 'Hospitality'
        MANUFACTURING = 12, 'Manufacturing'
        MEDIA = 13, 'Media'
        TELECOMMUNICATIONS = 14, 'Telecommunications'

        __empty__ = 'Empty'

    class DataForChoices(models.IntegerChoices):
        FOR_BUSINESS = 0, 'For business'
        PERSONAL = 1, 'Personal'

        __empty__ = 'Empty'

    class WhatWouldInterest(models.IntegerChoices):
        MARKET_RESEARCH = 0, 'Market research'
        PROVE_A_POINT = 1, 'Prove a point'
        COMPARE_OR_ANALYSE = 2, 'Compare/analyse'
        COLLECT_DATA = 3, 'Collect data'

        __empty__ = 'Empty'

    position = models.IntegerField(choices=PositionChoices.choices)
    role = models.IntegerField(choices=RoleChoices.choices)
    industry = models.IntegerField(choices=IndustryChoices.choices)
    data_for = models.IntegerField(choices=DataForChoices.choices)
    interest = models.IntegerField(choices=WhatWouldInterest.choices)

    class Meta:
        abstract = True
