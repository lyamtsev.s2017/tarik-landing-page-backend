from django.contrib import admin
from django.contrib.auth.models import Group, User
from import_export import resources
from import_export.admin import ImportExportModelAdmin, ExportActionMixin

from . import models

admin.site.unregister(Group)
admin.site.unregister(User)


class UserResource(resources.ModelResource):
    class Meta:
        model = models.User
        exclude = ('activation_token', 'modified')

    def dehydrate_position(self, instance):
        return instance.get_position_display()

    def dehydrate_role(self, instance):
        return instance.get_role_display()

    def dehydrate_industry(self, instance):
        return instance.get_industry_display()

    def dehydrate_data_for(self, instance):
        return instance.get_data_for_display()

    def dehydrate_interest(self, instance):
        return instance.get_interest_display()

    def dehydrate_is_active(self, instance):
        return 'Verified' if instance.is_active else 'Not verified'


@admin.register(models.User)
class UserAdmin(ExportActionMixin, admin.ModelAdmin):
    resource_class = UserResource
    readonly_fields = ('is_active',)

    class Meta:
        model = models.User
        verbose_name = 'Users'
