from django.urls import path
from . import views

app_name = 'user'

urlpatterns = [
    path('create', views.CreateUserView.as_view(), name='create-user'),
    path('verify', views.VerifyEmailView.as_view(), name='verify-email'),
]
