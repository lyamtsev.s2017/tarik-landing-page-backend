from django.db import models
from .abstract_models import TSModel, UserChoices


class User(TSModel, UserChoices):
    first_name = models.CharField('First name', max_length=256)
    last_name = models.CharField('Last name', max_length=256)
    phone = models.CharField('Phone number', max_length=256)
    language_preference = models.CharField('Language preference', max_length=256)
    email = models.EmailField('Email')
    age = models.IntegerField('Age')
    is_active = models.BooleanField('Has confirmed registration', default=False, editable=False)
    activation_token = models.CharField(max_length=256, editable=False)

    def __str__(self):
        return f'{self.last_name} {self.first_name}'
