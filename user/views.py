from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import send_mail
from django.template.loader import render_to_string
from rest_framework.exceptions import ValidationError
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import User
from .serializators import UserSerializer


class CreateUserView(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def perform_create(self, serializer):
        obj = serializer.save()
        domain = get_current_site(self.request).domain
        verification_url = f'https://{domain}/sign-up?token={obj.activation_token}'
        template = render_to_string('registration_confirmation.html', {"url": verification_url})
        mail = send_mail('Registration Confirmation', '',
                         settings.EMAIL_HOST_USER,
                         [obj.email],
                         fail_silently=False,
                         html_message=template)
        print(f'ONE CLICK MAIL SENT = {mail}')
        return obj


class VerifyEmailView(APIView):
    queryset = User.objects.filter(is_active=False)

    def post(self, request):
        token = request.data.get('token')
        if not token:
            raise ValidationError('Token is needed')

        user = self.queryset.filter(activation_token=token).last()

        if not user:
            return Response(404)
        else:
            user.is_active = True
            user.save()

        return Response(201)
