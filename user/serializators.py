import secrets
import time

from rest_framework.serializers import ModelSerializer

from .models import User


class UserSerializer(ModelSerializer):

    def create(self, validated_data):
        timestamp = time.time()
        confirmation_url = f'{timestamp}{secrets.token_urlsafe()}'
        validated_data['activation_token'] = confirmation_url
        return super().create(validated_data)

    class Meta:
        model = User
        exclude = ('is_active', 'activation_token')
