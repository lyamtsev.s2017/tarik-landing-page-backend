from .settings import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 465
EMAIL_HOST_USER = 'info@inphormable.com'
EMAIL_HOST_PASSWORD = '@Inphormable'
EMAIL_USE_SSL = True
EMAIL_USE_TLS = False

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static_files')

STATIC_DIR = os.path.join(BASE_DIR, 'static_files')

STATICFILES_DIRS = (os.path.join(BASE_DIR, 'static/'),)

CORS_ALLOW_ALL_ORIGINS = True
DEBUG = True
