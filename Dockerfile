FROM python:3.9-slim
RUN pip install --upgrade pip
WORKDIR /landing_page_backend

COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .
